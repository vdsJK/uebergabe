# Übergabe

## Artus Sanierung
[Webseite](https://artus-sanierung.info)
*Mitwirkende: Simon Hohnwald, Christina Pauls, Iris Siszer*

Die Seite ist online und vorerst gibt es nichts zu tun. 

---

## Wein Wolff - Freesia
[Webseite](http://dev.von-der-see.de/freesia/)
*Mitwirkende: Maike Immega, Iris Siszer*

Der aktuelle Stand geht heute (25.03.2019) an Jan raus und je nach Feedback wird die Seite online gestellt oder es müssen kleine Änderungen vorgenommen werden. An sich ist die Änderung aber fertig.

---

## Hotel Fährhaus
[Webseite](http://dev.von-der-see.de/hotel-faehrhaus/)
*Mitwirkende: Lena de Boer, Sebastian Freitag, Fabian Kielmann*

Hier warten wir auf Feedback von Herrn Fröhlich. Er sollte sich demnächst mit den Korrekturen melden, danach kann die Seite wahrscheinlich online.

---

## Hotel Deichkrone
[Webseite](http://dev.von-der-see.de/hotel-deichkrone/)
*Mitwirkende: Lena de Boer, Sebastian Freitag, Fabian Kielmann*

Hier warten wir auf Bilderinput von Herrn Fröhlich. Außerdem müssen noch einige Änderungen am Layout vorgenommen werden;
das kann aber auf den Bilder-Input warten, da bei den meisten Seiten ohne Bild das Layout verschoben ist. 
Die Buchungssoftware funktioniert soweit wie erwartet (auf den Unterseiten [Zimmerdetails](http://dev.von-der-see.de/hotel-deichkrone/zimmerdetails/), [Preisbeispiele](http://dev.von-der-see.de/hotel-deichkrone/preisbeispiele/) und [Last-Minute-Angebote](http://dev.von-der-see.de/hotel-deichkrone/last-minute-angebote/)), aber vielleicht hat Herr Fröhlich noch Anmerkungen. 

---

## Kanzlei Lotz
[Webseite](http://dev.von-der-see.de/kanzleilotz.de/)
*Mitwirkende: Christina Pauls(, Sebastian Freitag)*

Am Montag, 25.03.2019, sollte eine TK mit CP und JK stattfinden, die wurde aber erstmal verschoben. Die letzten Layout-Änderungen wurden übernommen und Boris ist an der Reihe, Content zu liefern. Boris weiß Bescheid, dass Joo weg ist; weitere Layoutänderungen können hoffentlich bis Mitte April warten.

---

## Veremderung
[Webseite](https://veremderung.de)
*Mitwirkende: Maike Immega, Iris Siszer*

---

## CEC/Agrowea
[Webseite_CEC](http://dev.von-der-see.de/cec-haren.de/), [Webseite_Agrowea](http://dev.von-der-see.de/agrowea)
*Mitwirkende: Maike Immega, Iris Siszer*

Auf Eis?